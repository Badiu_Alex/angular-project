import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  constructor(private element: ElementRef) {
    this.element.nativeElement.style.backgroundColor = "gray";
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.highlight('yellow');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlight('gray');
  }

  private highlight(color: string) {
    this.element.nativeElement.style.backgroundColor = color;
  }


}
