import {Component, OnInit} from '@angular/core';
import {AuthServiceService} from "../auth-service.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  emailValue = "";
  passwordValue = "";
  RePasswordValue = "";
  isShow = true;
  viewType: String = "login";

  constructor(private authService: AuthServiceService, private router: Router) {

  }

  ngOnInit(): void {
  }

  onLogin(): void {
    console.log(this.emailValue);
    console.log(this.passwordValue);
    // alert("Email: " + this.emailValue + " ,password: " + this.passwordValue);
    this.authService.login(this.emailValue, this.passwordValue).subscribe((response) => {
      console.log(response)
      this.router.navigate(["/dashboard"]);
    })
  }

  onRegister(): void {
    console.log(this.emailValue);
    console.log(this.passwordValue)
    console.log(this.RePasswordValue);
    // alert("Email: " + this.emailValue + " ,password: " + this.passwordValue + " ,RePassword: " + this.RePasswordValue);
    if (this.passwordValue == this.RePasswordValue) {
      console.log("Passwords match");
      this.authService.register(this.emailValue, this.passwordValue).subscribe((response) => {
        console.log(response)
      })
    } else {
      console.log("Passwords do not match");
    }
  }

  onShow(): void {
    // console.log("Before: " + this.isShow)
    // this.isShow = !this.isShow;
    // console.log("After: " + this.isShow)
    if(this.viewType=="login"){
      this.viewType = "register";
    } else {
      this.viewType = "login";
    }
  }
}
